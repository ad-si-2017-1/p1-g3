// Load the TCP Library 
net = require('net');

// Keep track of the chat clients 
var clients = []; 

// Mapa de Nickname
var nicks={};

// mautec: Salas que foram criadas pelos clientes
var Salas = ['#GERAL']; // mautec: sala padrao 

var user=[];
var senha=[];

// Start a TCP Server 
net.createServer (
function (socket) {   
	
    // Identify this client   
    socket.name = socket.remoteAddress + " : " + socket.remotePort   

    // Put this new client in the list   
    clients.push(socket);   

    // Send a nice welcome message and announce   
    socket.write ("Bem vindo " + socket.name + "!\n");
    broadcast("O usuário " +socket.name + " entrou no chat!\n", socket);

    // Handle incoming messages from clients.   
    socket.on ('data', 
        function (data) { analisar(data); }
    );

    // Remove the client from the list when it leaves   
    socket.on ('end', 
        function () {
            clients.splice(clients.indexOf(socket), 1);
            // mautec: Mostra o nick se este existir
            if (socket.nick)
                broadcast(socket.nick + " left the chat.\n");
            else
                broadcast(socket.name + " left the chat.\n");
        }
    );

    // Send a message to all clients   
    function broadcast(message, sender) {
        clients.forEach (
            function (client) {       
                if (client === sender) return;           // Don't want to send it to sender
                client.write(message);     }
        );

        process.stdout.write(message)                    // Log it to the server output too
    } 

    function analisar(data) {     
        var mensagem = String(data).trim();     
        var args     = mensagem.split(" ");              // o método split quebra a mensagem em partes separadas p/ espaço em branco
        var arg = args[0].toUpperCase();                 // mautec: deixando os comandos insensiveis ao caso
        args[0] = arg;

        if (args[0] == "NICK") nick(args);               // se o primeiro argumento for NICK
        else
        if (args[0] == "USER") user(args);
        else
        if (args[0] == "QUIT") quit(args);
        else
        if (args[0] == "PASSWORD") password(args);            
        else
        if (args[0] == "JOIN") join(args);
        else
        if (args[0] == "PART") part(args);
        else
        if (args[0] == "LIST") list(args);
        else
        if (args[0] == "PRIVMSG") {
            // mautec: Verificando a presenca de ":"
            arg_composto = mensagem.split(":");
            arg  = arg_composto[1]; // remove o item na posicao 0
            if (arg) args[2] = arg; // msg  contem ":". alterar args[2] para incluir espacos
            privmsg (args); }
		else
		if (args[0] == "MODE") userMode (args);
		else
		if (args[0] == "WHO") who(args);
		else
		if (args[0] == "AWAY") away(args);
        else socket.write("ERRO: comando inexistente\n");   
    } 
    
    function sendNick(nick, cb, authd) {
            var auth_errors = [
                'ERR_NONICKNAMEGIVEN',
                'ERR_ERRONEUSNICKNAME',
                'ERR_NICKNAMEINUSE',
                'ERR_NICKCOLLISION',
                'ERR_UNAVAILRESOURCE',

            ];

            function error(data) {
                if (cb) cb.call(this, data.command, nick);
                removeListeners.call(this);
            }

            function removeListeners() {
                for (var i = 0; i < auth_errors.length; i++)
                    this.removeListener(auth_errors[i], error);
                if ('function' === typeof authd) this.removeListener('NOTICE', authd);
            }

            
            for (var i = 0; i < auth_errors.length; i++)
                this.once(auth_errors[i], error);

            this.send('NICK ', nick);
        }

    // Função nick atualizada!
    function nick(args) {                                                                                
        if ( ! args[1] ) { // se o segundo argumento for vazio                                         
                socket.write("ERRO: nickname faltando\n");                                             
                return; // retorna da função, não executa depois daqui                                 
        }                                                                                              
        else if ( nicks [ args[1] ] ) { // procura o nickname no mapa                                  
                socket.write("ERRO: nickname já existe\n");                                            
                return;                                                                                
        }                                                                                              
        else { // registra o nickname no mapa                                                          
                if ( socket.nick ) {    // o nickname já foi atribuído?                                
                        // remove nickname atual no mapa                                               
                        delete nicks[ socket.nick ];                                                   
                }                                                                                     
                // inclui o nickname no mapa                                                           
                nicks [ args[1] ] = socket.name;                                                       
                                                                                                       
                // Pega o nick anterior para exibir a partir da segunda troca de nick                                                
                if ( socket.nick ) { 
                 	socket.name = socket.nick;
                 }
        		// registra o nickname no objeto socket
                socket.nick = args[1];       
                
        }                                                                                              
        socket.write("OK: comando NICK executado com sucesso\n");                                      
        texto = "\nO usuario " + socket.name + " agora é conhecido por " + socket.nick + "\n";
        broadcast (texto);
        
    }                           
  
    function user(args) { 
		if ( ! args[1] ) {                                          
                socket.write("ERRO: Você não digitou nenhum nome para usuário!\n");                                             
                return;                                  
        }  
        if ( ! args[2] ) {                                          
                socket.write("ERRO: Você não digitou a senha para usuário!\n");                                             
                return;                                  
        }                                                                                              
                                                                                                  
        var criarUser = true;
              		                              
        	for (var i=1; i<= user.length; i++){
        		//verifica se o usuário existe
        		
        		if (user[i] == args[1]){
        			criarUser = false;
        			//verifica se a senha está correta
        			if (senha[i] == args[2]){
        				socket.write("Usuário logado com sucesso!\n");
        				socket.user = args[1]; 
        				return; 
        			} else {
        				socket.write("Senha inválida.\n");
        				return;
        			}
        		}
        	}
			
			//cria o usuário se ele não existir        	   
        	if (criarUser){                                                                       
                socket.user = args[1];  
                user[user.length] = (socket.user);
                senha[user.length] = args[2];
                socket.write ("OK: Usuario cadastrado com sucesso!\n");
                socket.write("user/senha: " + user[user.length] + "/" + senha[senha.length] + "\n");
            }

        
    } 
    
    //desconecta o cliente
    function quit(args) { 
		
		
		texto = "\nO usuario " + socket.nick + " saiu!\n";
		broadcast (texto);
		
		delete socket.nick;
		delete socket.name;
		
		socket.end();                                

    }

    function list(args) {
        // <channel> *( "," <channel> ) [ <target> ]
        // mautec: como ainda nao temos topicos para a sala
        // o comando apenas lista as salas existentes
        var sl;
        for (sl in Salas)
            socket.write (Salas[sl] + '\n');
    }
    
    function join(args) { 
        // <channel> *( "," <channel> ) [ <key> *( "," <key> ) ]
        // mautec: Verificar existencia da lista de salas e criar uma se necessario
        if (! socket.Channels) {
            socket.Channels = [];
        }

        // mautec: Verifica se o usuario deseja sair de todas as Salas "0"
        if (args[1] == "0") {
            console.log ("OK: usuario deseja sair de todas as Salas\n");
            socket.Channels = [];
            // executar funcao PART <sala> em todas as Salas em socket.Channels
            return;
        }

        // mautec: APENAS PARA TESTES - APRESENTA AS SALAS QUE O USUARIO FEZ JOIN
        if (args[1] == "*") {
            if (! socket.Channels.length) {
                socket.write ("Nenhuma Sala\n");
                return;
            }

            Channels = socket.Channels.join("|");
            socket.write ("\nSalas que o usario fez JOIN: ");
            socket.write (Channels);
            return;
        }

        // mautec: Configurar uma lista das salas que o usuario deseja entrar
        if (! args[1]) { // cmd JOIN deve fornecer a lista das salas
            socket.write ("ERRO: commando JOIN inválido!\n");
            return;
        }

        // mautec: Configurar uma lista das chaves a serem utilizadas em cada sala
        // mautec: AINDA NAO SEI PARA QUE SERVEM AS CHAVES....
        if (args[2])
            Chaves= args[2];
        else
            Chaves= [];

        // mautec: Armazenar lista de salas que o usuario participa
        Channels        = args[1].split(",");
        socket.Channels = Channels;

        // mautec: ATUALIZAR OU INSERIR A NOVA SALA NA RELACAO DE SALAS
        var sl, ch;
        var found = 0;
        var novas = []; // armazenas as novas salas

        // percorre as salas solicitadas
        for (ch in Channels) { 
             // e verifica presenca em Salas
            found = 0;
            for (sl in Salas)
                if (Salas[sl] == Channels[ch]) {
                    texto =  "ENTROU NA SALA ";
                    broadcastSala(Salas[sl], texto);
                    found = 1;
                    break; }
            // caso nao encontrado, registra nova sala
            if (! found) novas.push   (Channels[ch]);
        }

        // armazena as novas salas
        for (sl in novas) Salas.push (novas[sl]);

        console.log ("OK: comando JOIN executado com sucesso\n");

    }//join

    function part(args) { 
        // <channel> *( "," <channel> ) [<leave msg>] 
        // mautec: Verificar existencia da lista de salas
        if (! socket.Channels) {
            console.log("PART: nenhuma sala foi criada" );
            return;
        }

        // mautec: Verifica se o usuario deseja sair de todas as Salas "0"
        if (args[1] == "0") {
            console.log ("OK: usuario deseja sair de todas as Salas\n");
            socket.Channels = [];
            // executar funcao PART <sala> em todas as Salas em socket.Channels
            return;
        }

        // mautec: Configurar uma lista das salas que o usuario deseja sair
        if (! args[1]) { // cmd PART deve fornecer a lista das salas
            socket.write ("ERRO: commando PART inválido!\n");
            return;
        }

        // mautec: lista de salas para sair
        Channels = args[1].split(",");


        var sl, ch;
        var found = 0;
        var novas = []; // armazenas as remanescentes

        // percorre as salas solicitadas
        for (ch in Channels) { 
             // e verifica presenca em socket.Channels
            found  = 0;
            indice = socket.Channels.indexOf(Channels[ch]);

            if (indice != -1) {
                socket.Channels.splice(indice, 1); // remove a sala
                }
        }// for ch

        console.log ("OK: comando PART executado com sucesso\n");

    }//PART
/*
ERR_NORECIPIENT
ERR_NOTEXTTOSEND
ERR_CANNOTSENDTOCHAN
ERR_NOTOPLEVEL
ERR_WILDTOPLEVEL
ERR_TOOMANYTARGETS
ERR_NOSUCHNICK
RPL_AWAY
*/
    // mautec:  REALIZA ENVIO DE MENSAGEM
    // formato: <msgtarget> <text to be sent>
    function privmsg (args) {

        // mautec: VERIFICANDO PARAMETROS DA MSG
        if (! args[0] || ! args[1]) {
            socket.write ("ERRO: commando PRIVMSG inválido (PRIVMSG <msgalvo> <msg>\n");
            return;
        }

        console.log(args);

        // mautec: IDENTIFICANDO O DESTINATARIO: PODE SER UM NICK OU UMA SALA
        Destino = args[1];

        // mautec: ENVIA A MSG PARA UM NICK
        var nickEncontrado = 0;
        clients.forEach (
            function (client) {       
                if (client.nick === socket.nick) return;           // Don't want to send it to sender
                if (client.nick === Destino) {
                    nickEncontrado = 1;
                    client.write(socket.nick + ": " + args[2] + "\n"); 
                    console.log ("OK: comando PRIVMSG executado com sucesso\n");
                    return;
                }
            }
        );

        if (nickEncontrado) return;

        // mautec: ENVIA A MSG PARA UMA SALA CASO NAO TENHA ENVIADO PARA UM NICK
        // PROCURA POR DESTINA NA RELACAO DE SALAS - DEPOIS DE ENCONTRADO 
        // PROCURA POR CLIENTES REGISTRADOS NA SALA
        salaEncontrada = 0;
        socket.Channels.forEach (
            function (sala) {
                if (sala === Destino) {
                    salaEncontrada = 1;
                    console.log ( "PRIVSMG: ENVIANDO MSG PARA A SALA " + sala + "\n");
                    broadcastSala (Destino, args[2]);
                }// sala == destino
            } // para cada sala...
        );

        if (! salaEncontrada) 
            socket.write ("PRIVMSG: ERRO SALA " + args[1] + " NAO ENCONTRADA\n");

        console.log ("OK: comando PRIVMSG executado com sucesso\n");

    }//privmsg

	function broadcastSala (Destino, msg) {
        Salas.forEach (
            function (sala) {
                if (sala === Destino) {
                    // VERIFICAR CADA USUARIO CADASTRADO SE ESTE FEZ JOIN NA SALA
                    clients.forEach (
                        function (client) {
                        var sl;
                            if (client.nick === socket.nick) return;           // Don't want to send it to sender
                            for (sl in client.Channels)
                                if (client.Channels[sl] === sala) {
                                    client.write ("[" + Destino+"] " + socket.nick + ": " + msg + "\n");
                                    break; }
                        } // para cada cliente
                    );
                }// sala == destino
            } // para cada sala...
        );
    }

	// Atribui Modes para os usuários
	function userMode (args) {
		
		//verifica se existe os comandos necessários
		if (!args[1] || !args[2]) {
			socket.write ("ERRO: commando MODE inválido (MODE <nickname> +mode[s]\n");
            return;
		}
		//valida se o nickname passado é válido
		if (!nicks [ args[1] ]){
			socket.write ("ERRO: nickname inválido!\n");
			return;
		}
		//valida se o(s) mode(s) são válidos
		if(!valideModes(args[2])){
			socket.write("ERRO: mode(s) utilizado(s) inválido(s)!\n");
			return;
		}
		
		//Adiciona os modes ao cliente(nickname) informado
		clients.forEach (
            function (client) {       
                if (client.nick === args[1]) {
					client.modes = args[2];
					socket.write("OK: Mode(s) " +args[2] +" aplicado ao cliente " + client.nick + "\n");
				}
            }
        );
	}
	
	// Verifica quais clientes estão no canal/sala passado
	function who(args) {
		//verifica se existe os comandos necessários
		if (!args[1]) {
			socket.write ("ERRO: commando WHO inválido (WHO #<canal>)\n");
            return;
		}
		
		//valida se a Sala existe
        // mautec: alteracao => eh preciso encontrar o indice primeiro
        var index = Salas.indexOf(args[1]);
		//if (!Salas[ args[1] ]){
        if (index == -1) {
			socket.write ("ERRO: Sala inválida!\n");
			return;
		}	
		
		Salas.forEach (
            function (sala) {
                if (sala === args[1]) {
                    // Verificar quais usuarios estão na sala
                    clients.forEach (
                        function (client) { 
                            for (sl in client.Channels) {
                                if (client.Channels[sl] === args[1]) {
									socket.write(client.nick + "\n");
								}	
							}
                        }
                    );
                }
                 else console.log(sala + " SALA " + args[1] + " NAO ENCONTRADA");
            }
        );		
	}
	
	//Define o status AWAY/Ausente para o cliente que o executou
	function away(args) {
		
		//Define o status do cliente
		socket.status = "AWAY";
		
		var mensagem = socket.status + "> ";
		if(socket.nick) {
			mensagem += socket.nick;
		} else {
			mensagem += socket.name;
		}
		
		//verifica se existe alguma mensagem e salva ela no cliente
		if (args[1]) {
			socket.mensagemAWAY = args[1];
			mensagem += " Mensagem: " + socket.mensagemAWAY;
		}
		
		mensagem += "\n";
		
		//Informa aos demais clientes
		broadcast(mensagem, socket);		
	}

} ).listen(6667); // function (socket)


// Put a friendly message on the terminal of the server.
console.log("<< Chat usando protocolo IRC >>\n");
console.log("O servidor está rodando na porta 6667\n");

// visualizar os dados como são (sem tratamento)     
// console.log(data);     
// visualizar como string     
// console.log(String(data));     
// visualizar os caracteres especiais enviados     
// console.log(JSON.stringify(String(data)));     
// o método trim remove os caracteres especiais do final da string
// Para testar o cliente: telnet localhost 6667
